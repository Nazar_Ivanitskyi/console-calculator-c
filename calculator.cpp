#include "math.h" // ���� ���������� �������� ��� ��������� ������������ �����
#include "iostream" // ���� ��� �������� ���������
using namespace std; // �������� �����. ������ ����������� ����.
int main(void) { // ������� main
	setlocale(LC_ALL, ""); //������������ ����
	float int_1, int_2, result; // ����� 1, ����� 2, ���������
	char operand; //���� ��������
	cout << "����i�� ����� �� ���� ������i�: " << endl; //�������� ����� "����i�� ����� �� ���� ������i�: "
	cout << "����� 1: "; cin >> int_1; //������� ����� 1
	cout << "���� ������i�: "; cin >> operand; //������� ���� ��������
	cout << "����� 2: "; cin >> int_2; //������� ����� 2
	bool processing = true; //�������� �� ��������� �������� ����� ��������
	switch (operand) { //������������ ����� �������� 
		case '+': result = int_1 + int_2; break; //ĳ� ��� ���������
		case '-': result = int_1 - int_2; break; //ĳ� ��� ���������
		case '*': result = int_1 * int_2; break; //ĳ� ��� ��������
		case '/': result = int_1 / int_2; break; //ĳ� ��� ������
		case '^': result = pow(int_1, int_2); break; // ĳ� ��� ��������� �� �������   
		default : {
			cout << "Error: ���i����� ���� ������i�!" << endl;  //�������� ����������� ��� �������(�� ���������� ���� ��������)
			processing = false; //���� �������� - �� ����������
			return 0;
		}
	}
	if(processing) {//���� ��������� ������ ���� �������� ����������
		cout << "���������: " << result << endl; //�������� ����������� � �����������
		cout << "Developed by Nazar Ivanitskyi (Processing.) | My first programm on C++ :)" << endl; //Copyright
		system("PAUSE"); // ��������� ��������, ��� �� �������� ��, ��� ��������� ����������.
		return 0; // ��������� �������� 0
	}
}  
